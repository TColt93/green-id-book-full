from digit_extract import digit_extraction_full as digits_extract
import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import json
import re
import imutils
import sys
from PIL import Image
import dimensions as dimensions
import boundaries as bounds
import base64



def Barcode_Thresh(img, cnt):
    boundaries, emblem_boundaries, barcode_boundaries = bounds.get_boundaries(3)
    if cnt != 0:
        cnt = cnt*5
        barcode_boundaries[0][1][0] = barcode_boundaries[0][1][0] - cnt
        barcode_boundaries[0][1][1] = barcode_boundaries[0][1][1] - cnt
        barcode_boundaries[0][1][2] = barcode_boundaries[0][1][2] - cnt

    for (lower, upper) in barcode_boundaries:
        lower = np.array(lower, dtype = 'uint8')
        upper = np.array(upper, dtype = 'uint8')

        (h,w) = img.shape[:2]
        blank_image = np.zeros((h,w,3), np.uint8)

        # find the colors within the specified boundaries and apply the mask
        mask = cv2.inRange(img, lower, upper)
        img = cv2.bitwise_not(img, blank_image, mask = mask)

        ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)
    return img

# Functionality for extracting the digits from the barcode DoB area
def barDoB_Fetch(barDoB, type):
    barcodeTrue = 'True'
    digits, out = digits_extract(barDoB, type, 1)
    digitsInternal = digits
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        barcodeTrue = 'Could Not Validate'
        return barcodeTrue, digits

    matcher = 0
    cntr = 0
    while cntr is not 2:
        matchspace = re.search(r' ', digits)
        if matchspace is not None:
            m1 = digits[matchspace.regs[0][0]]
            m2 = digits[matchspace.regs[0][1]]
            if  m2 == ' ':
                matcher = matchspace.regs[0][1]
            elif m1 == ' ':
                matcher = matchspace.regs[0][0]
            if cntr == 1:
                digitsInternal = digits[:matcher]
            else:
                before = len(digits[:matcher])
                after = len(digits[matcher:])
                if before > after:
                    digitsInternal = digits[:matcher]
                else:
                    digitsInternal = digits[matcher+1:]
        cntr += 1

    #Make sure that we are focusing on the right second last digit
    length = len(digitsInternal)
    if length > 6:
        digitsInternal = digitsInternal[:6]

    if length < 6:
        barcodeTrue = 'Could Not Validate'

    if barcodeTrue != 'Could Not Validate':
        if digitsInternal[0] in ['0','1','2']:
            digitsInternal = '20' + digitsInternal[0] + digitsInternal[1] + '-' + digitsInternal[2] + digitsInternal[3] + '-' + digitsInternal[4] + digitsInternal[5]
            barcodeTrue = digitsInternal
            return barcodeTrue, digits
        else:
            digitsInternal = '19' + digitsInternal[0] + digitsInternal[1] + '-' + digitsInternal[2] + digitsInternal[3] + '-' + digitsInternal[4] + digitsInternal[5]
            barcodeTrue = digitsInternal
            return barcodeTrue, digits

    return barcodeTrue, digits

# Functionality for extracting the digits from the DoB area
def DoB_Fetch(digits, type):
    DateTrue = 'True'
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        DateTrue = 'Could Not Validate'
        dob_result = DateTrue
        return dob_result

    digits = digits.replace('-', '')

    length = len(digits)

    if length < 6:
        DateTrue = 'Could Not Validate'
        dob_result = DateTrue
        return dob_result

    if  DateTrue != 'Could Not Validate':
        DateTrue = digits
        dob_result = DateTrue
        return dob_result

def date_check(barcodeTrue, DateTrue):
    DateCheck = 'Could Not Validate'
    if barcodeTrue == DateTrue:
        DateCheck = 'True'
        date_result = DateCheck
        return date_result
    bar = barcodeTrue.replace('-','')
    date = DateTrue.replace('-','')
    if bar == date:
        DateCheck = 'True'
        date_result = DateCheck
        return date_result
    lengthD = len(date)
    lengthB = len(bar)
    if lengthD == 10:
        date0 = date[:4]+date[5:7]+date[lengthD-2:]
        if bar == date0:
            DateCheck = 'True'
            date_result = DateCheck
            return date_result
        else:
            count = 0
            charCount = 0
            if charCount <= len(date)-1:
                for Bchar in bar:
                    if Bchar == date[charCount]:
                        count += 1
                        charCount += 1
                        continue
                    elif Bchar == date[charCount+1] and charCount != 9:
                        count += 1
                        charCount += 2
                    else:
                        charCount += 1
        if count > 6:
            DateCheck = 'True*'
            date_result = DateCheck
            return date_result
    if lengthD == 9:
        date1 = date[:6]+date[lengthD-2:]
        if bar == date1:
            DateCheck = 'True'
            date_result = DateCheck
            return date_result
        date1 = date[:4]+date[5:]
        if bar == date1:
            DateCheck = 'True'
            date_result = DateCheck
            return date_result
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                if charCount <= len(date)-1:
                    d = date[charCount]
                    if Bchar == d:
                        count += 1
                        charCount += 1
                        continue
                    d1 = date[charCount + 1]
                    if Bchar == d1 and charCount != 8:
                        count += 1
                        charCount += 2
                    else:
                        charCount += 1
        if count > 6:
            DateCheck = 'True*'
            date_result = DateCheck
            return date_result
    if lengthD == 8:
        if bar == date:
            DateCheck = 'True'
            date_result = DateCheck
            return date_result
        else:
            count = 0
            charCount = 0
            for Bchar in bar:
                if charCount <= len(date)-1:
                    if Bchar == date[charCount]:
                        count += 1
                        charCount += 1
                        continue
                    if charCount != 7:
                        if Bchar == date[charCount+1]:
                            count += 1
                            charCount += 2
                    else:
                        charCount += 1
        if count > 6:
            DateCheck = 'True*'
            date_result = DateCheck
            return date_result
    date2 = date[:2]+date[3:5]+date[lengthD-2:]
    bar2 = bar[:2]+bar[4:6]+bar[lengthB-2:]
    if bar2 == date2:
        DateCheck = 'True'
        date_result = DateCheck
        return date_result
    date3 = date[:4]+date[5]+date[lengthD-2:]
    bar3 = bar[:4]+bar[5]+date[lengthB-2:]
    if bar3 == date3:
        DateCheck = 'True'
        date_result = DateCheck
        return date_result
    date_result = DateCheck
    return date_result



def ExtraDocument_Validate(LastThreeImg, type):
    digits, out = digits_extract(LastThreeImg, type, 1)
    output =  out
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could Not Validate'
        extra_result = result
        return extra_result, digits
    matchspace = re.search(r' ', digits)
    matchdot = re.search(r'.', digits)
    matchdash = re.search(r'-', digits)
    if (matchspace is not None) or (matchdot is not None) or (matchdash is not None):
        digits = digits.replace(' ', '')
        digits = digits.replace('.', '')
        digits = digits.replace('-', '')

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    secondLast = digits[(length - 2)]

    if secondLast == '8':
        result = 'NO'
        extra_result = result
        return extra_result, digits
    elif secondLast == '9':
        result = 'YES'
        extra_result = result
        return extra_result, digits
    else:
        result = 'Could not Validate'
        extra_result = result
        return extra_result, digits

def Gender_Validate(digits, type):
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could not Validate'
        gender_result = result
        return gender_result
    
    digits = digits[length-4:]

    matchspace = re.search(r' ', digits)
    if matchspace is not None:
        m1 = digits[matchspace.regs[0][0]]
        m2 = digits[matchspace.regs[0][1]]
        if  m2 == ' ':
            matcher = matchspace.regs[0][1]
        elif m1 == ' ':
            matcher = matchspace.regs[0][0]
        digits = digits[matcher+1:]

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    firstDigit = digits[0]

    if firstDigit < '5':
        result = 'Female'
        gender_result = result
        return gender_result
    elif firstDigit >= '5':
        result = 'Male'
        gender_result = result
        return gender_result
    elif firstDigit == ' ':
        firstDigit = digits[1]
        if firstDigit < '5':
            result = 'Female'
            gender_result = result
            return gender_result
        elif firstDigit >= '5':
            result = 'Male'
            gender_result = result
            return gender_result

def Nationality_Validate(digits, type):
    # find length of digits string
    length = len(digits)
    # Move onto the next one if cannot find any digits
    if length == 0 or length == 1:
        result = 'Could not Validate'
        nationality_result = result
        return nationality_result

    #Make sure that we are focusing on the right second last digit
    length = len(digits)
    ThirdLast = digits[(length - 3)]

    if ThirdLast == '0':
        result = 'SA Citizen'
        nationality_result = result
        return nationality_result
    elif ThirdLast == '1':
        result = 'Permanent Residence'
        nationality_result = result
        return nationality_result
    else:
        result = 'Could not Validate'
    nationality_result = result
    return nationality_result
from text_extract import text_extraction_best as text_extract
from digit_extract import digit_extraction_full as digits_extract
import cv2
import os
import numpy as np
import datetime
from skimage.measure import compare_ssim
import re
import sys
import dimensions as dimensions
import boundaries as bounds
import imutils
import base64
import Green_ID_Validations as Validations

class GreenID_OCR():
    def __init__(self):
        self.images = []
        self.predictions = []
    # Initialize variables needed
    currentScore = 0
    count = 0

    # Start the Extraction of RoIs from the Whole ID book
    def Extract_ROIs(self, image, Type):
        # Get the dynamic dimensions from the get_dimensions function
        RoIs = dimensions.get_dimensions(1)
        CoBDim = RoIs[1]
        DoIDim = RoIs[2]
        DoBDim = RoIs[3]
        NameDim = RoIs[4]
        SurnameDim = RoIs[5]

        # Get ID width and height
        (h, w) = image.shape[:2]

        #Extract and Write CoB
        CoBPic = image[round(int(h)*CoBDim[Type][0]) : round(int(h)*CoBDim[Type][1]), round(int(w)*CoBDim[Type][2]) : round(int(w)*CoBDim[Type][3])]

        #Extract and Write DoIs
        DoIPic = image[round(int(h)*DoIDim[Type][0]) : round(int(h)*DoIDim[Type][1]), round(int(w)*DoIDim[Type][2]) : round(int(w)*DoIDim[Type][3])]

        #Extract and Write DoBs
        DoBPic = image[round(int(h)*DoBDim[Type][0]) : round(int(h)*DoBDim[Type][1]), round(int(w)*DoBDim[Type][2]) : round(int(w)*DoBDim[Type][3])]

        #Extract and Write Names
        NamePic = image[round(int(h)*NameDim[Type][0]) : round(int(h)*NameDim[Type][1]), round(int(w)*NameDim[Type][2]) : round(int(w)*NameDim[Type][3])]

        #Extract and Write SurNames
        SurnamePic = image[round(int(h)*SurnameDim[Type][0]) : round(int(h)*SurnameDim[Type][1]), round(int(w)*SurnameDim[Type][2]) : round(int(w)*SurnameDim[Type][3])]
        
        self.rois = [CoBPic, DoIPic, DoBPic, NamePic, SurnamePic]

    # Threshold the ROIs using colour coundaries
    def ROI_Thresh(self, Type):
        Threshed = []
        boundaries = bounds.get_boundaries(1)
        boundary = []
        boundary.append(boundaries[Type])
        for file in self.rois:
            imgLoad = file
            for (lower, upper) in boundary:
                v = np.median(imgLoad)

                # Get the right color median threshold
                if v >= 190:
                    v = v - 75
                elif v >= 175:
                    v = v - 70
                elif v >= 150:
                    v = v - 50
                elif v >= 110:
                    v = v - 45
                elif v < 110:
                    v = 50

                upper = [v,v,v]
                # create NumPy arrays from the boundaries
                lower = np.array(lower, dtype = 'uint8')
                upper = np.array(upper, dtype = 'uint8')

                (h,w) = imgLoad.shape[:2]
                blank_image = np.zeros((h,w,3), np.uint8)

                # find the colors within the specified boundaries and apply the mask
                mask = cv2.inRange(imgLoad, lower, upper)
                img = cv2.bitwise_not(imgLoad, blank_image, mask = mask)

                # Threshold RoI for sorting
                ret, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)

            Threshed.append(img)

        del self.rois
        self.threshed = Threshed

        # cv2.imshow('cob', self.threshed[0])
        # cv2.imshow('doi', self.threshed[1])
        # cv2.imshow('dob', self.threshed[2])
        # cv2.imshow('name', self.threshed[3])
        # cv2.imshow('sur', self.threshed[4])
        # cv2.waitKey(0)

    # Extract the text from the threshed ROIs
    def Extract_Characters(self, type):
        Text = []
        output = []
        count = 0
        for file in self.threshed:
            count += 1
            # load the example image and convert it to grayscale
            if (count == 1) or (count == 4) or (count == 5):
                #OCR the Text from the image given
                Name, out = text_extract(file, type)

                if Name is not '':
                    Text.append(Name)
                    output.append(out)
                else:
                    Text.append('Could Not Read Text')
                    output.append(out)
            else:
                #OCR the Dates from the image given
                digits, out = digits_extract(file, type, 0)

                # find length of digits string
                length = len(digits)
                # Move onto the next one if cannot find any digits
                if length == 0 or length == 1:
                    Text.append('Could not Read Date')
                    output.append(out)
                    continue

                # Understand how many characters were found and organise them into their matching string
                if digits is not '':
                    if len(digits) == 10:
                            digits = digits[:4]+ '-' + digits[5:7] + '-' + digits[8:]
                    if len(digits) == 9:
                        if digits[4] == '7' or digits[4] == '2':
                            digits = digits[:4]+ '-' + digits[5:7] + '-' + digits[7:]
                        if digits[6] == '7' or digits[6] == '2':
                            digits = digits[:4]+ '-' + digits[4:6] + '-' + digits[7:]
                    if len(digits) == 8:
                        digits = digits[:4]+ '-' + digits[4:6] + '-' + digits[6:]
                    digits = digits.replace(' ', '-')
                    Text.append(digits)
                    output.append(out)
                else:
                    Text.append('Could not Read Date')
                    output.append(out)

        txt = Text_maker(Text)
        
        self.predictions = txt
        return output

    # Functionality for finding the emblems and counting how many correct matches in the ID Book
    def find_and_validate_emblems(self, image, Type):
        # Create Variables for the paths and image areas
        Emblempath = 'Templates/Emblem.png'
            
        boundaries, emblem_boundaries, barcode_boundaries = bounds.get_boundaries(3)

        RoIs, Barcodes, Emblems = dimensions.get_dimensions(3)
        Emblem1Dim = Emblems[0]
        Emblem2Dim = Emblems[1]
        Emblem3Dim = Emblems[2]
        Emblem4Dim = Emblems[3]

        totalFoundCount = 0
        EmblemPics = []

        img = image
        (h, w) = img.shape[:2]

        EmblemPics.append(img[round(int(h)*Emblem1Dim[0]) : round(int(h)*Emblem1Dim[1]), round(int(w)*Emblem1Dim[2]) : round(int(w)*Emblem1Dim[3])])
        EmblemPics.append(img[round(int(h)*Emblem2Dim[0]) : round(int(h)*Emblem2Dim[1]), round(int(w)*Emblem2Dim[2]) : round(int(w)*Emblem2Dim[3])])
        EmblemPics.append(img[round(int(h)*Emblem3Dim[0]) : round(int(h)*Emblem3Dim[1]), round(int(w)*Emblem3Dim[2]) : round(int(w)*Emblem3Dim[3])])
        EmblemPics.append(img[round(int(h)*Emblem4Dim[0]) : round(int(h)*Emblem4Dim[1]), round(int(w)*Emblem4Dim[2]) : round(int(w)*Emblem4Dim[3])])

        for img in EmblemPics:

            found = [0]
            foundCount = 0

            for (lower, upper) in emblem_boundaries:
                # create NumPy arrays from the boundaries
                lower = np.array(lower, dtype = 'uint8')
                upper = np.array(upper, dtype = 'uint8')

                # find the colors within the specified boundaries and apply the mask
                mask = cv2.inRange(img, lower, upper)
                img = cv2.bitwise_and(img, img, mask = mask)

                kernel = np.ones((2,2),np.uint8)
                img = cv2.dilate(img,kernel,iterations = 1)

                img = cv2.bitwise_not(img)
                ret, img = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY)
                img = cv2.bitwise_not(img)
                
            #Get image dimensions for template resizing and get canny edges for ROI in image
            (w, h) = img.shape[:2]
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.Canny(img, 50, 200)

            #Template read and manipulate to fit Image dimensions and get canny edges of template image
            template = cv2.imread(Emblempath)
            width = round(w*0.3)
            height = round(h*0.5)
            template = cv2.resize(template, (width, height))
            template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
            template = cv2.Canny(template, 50, 200)
            (tH, tW) = template.shape[:2]

            for scale in np.linspace(0.2, 1.0, 20)[::-1]:
                # resize the image according to the scale, and keep track of the ratio of the resizing
                resized = imutils.resize(img, width = int(img.shape[1] * scale))
                r = img.shape[1] / float(resized.shape[1])

                # if the resized image is smaller than the template, then break from the loop
                if resized.shape[0] < tH or resized.shape[1] < tW:
                    break

                # detect edges in the resized, grayscale image and apply template matching to find the template in the image
                result = cv2.matchTemplate(resized, template, cv2.TM_CCOEFF)
                (_, maxVal, _, maxLoc) = cv2.minMaxLoc(result)

                # if we have found a new maximum correlation value, then update the bookkeeping variable
                if found is None or maxVal > found[0]:
                    found = (maxVal, maxLoc, r)

            # unpack the bookkeeping varaible and compute the (x, y) coordinates of the bounding box based on the resized ratio
            if found[0] > 850000:
                foundCount = foundCount + 1

            totalFoundCount = totalFoundCount + foundCount
        self.emblems_found = totalFoundCount

    # Functionality for validating the dates and numbers within the ID Book
    def Barcode_Validation(self, image, DoBimage, Type):

        RoIs, Barcodes, Emblems = dimensions.get_dimensions(3)
        BarcodesDim = Barcodes[0]
        DigitsDim = Barcodes[1]
        LastThreeDim = Barcodes[2]
        DateCheckDim = Barcodes[3]
        BarcodeDim = Barcodes[4]

        #Extract all the RoIs from the Barcode
        (h, w) = image.shape[:2]
        BarcodePicOrig = image[round(int(h)*BarcodesDim[Type][0]) : round(int(h)*BarcodesDim[Type][1]), round(int(w)*BarcodesDim[Type][2]) : round(int(w)*BarcodesDim[Type][3])]        

        black = 0
        cnt = 0
        c = 0
        while (black < 35000 or black > 38200) and c < 30:
            BarcodePic = Validations.Barcode_Thresh(BarcodePicOrig, c)
            colorLow = np.array([50, 50, 50], np.uint8)
            colorHigh = np.array([255, 255, 255], np.uint8)

            if Type == 1:
                testImage = BarcodePic[int(0.04*h):h,0:int(w*0.85)]
            if Type == 0:
                testImage = BarcodePic[0:h,0:int(w*0.85)]

            dst = cv2.inRange(testImage, colorLow, colorHigh)
            black = cv2.countNonZero(dst)

            # cv2.imshow(str(black), testImage)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            if black > 35000 and black < 38200:
                break
            elif black > 38200:
                cnt += 1
                c = cnt*-1
            else:
                cnt += 1
                c = cnt
        
        # cv2.imshow('barcode', BarcodePic)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows

        (h2, w2) = BarcodePic.shape[:2]
        DigitsPic = BarcodePic[round(int(h2)*DigitsDim[Type][0]) : round(int(h2)*DigitsDim[Type][1]), round(int(w2)*DigitsDim[Type][2]) : round(int(w2)*DigitsDim[Type][3])]
        (h3, w3) = DigitsPic.shape[:2]
        LastThreePic = DigitsPic[round(int(h3)*LastThreeDim[Type][0]) : round(int(h3)*LastThreeDim[Type][1]), round(int(w3)*LastThreeDim[Type][2]) : round(int(w3)*LastThreeDim[Type][3])]
        DateCheckPic = DigitsPic[round(int(h3)*DateCheckDim[Type][0]) : round(int(h3)*DateCheckDim[Type][1]), round(int(w3)*DateCheckDim[Type][2]) : round(int(w3)*DateCheckDim[Type][3])]
        BarDigitPic = DigitsPic[round(int(h3)*BarcodeDim[Type][0]) : round(int(h3)*BarcodeDim[Type][1]), round(int(w3)*BarcodeDim[Type][2]) : round(int(w3)*BarcodeDim[Type][3])]
        
        # cv2.imshow('barcode', BarcodePicOrig)
        # cv2.imshow(str(black), BarcodePic)
        # cv2.waitKey(0)
        
        idNumber, out = digits_extract(BarDigitPic, Type, 1)
        if len(idNumber) < 8:
            self.idNumber = '0'
        else:
            self.idNumber = idNumber.replace(' ', '')

        self.barcode_pic = BarcodePicOrig

        # Get the DoB from the ID and the Barcode areas
        self.bardob_result, date_digits = Validations.barDoB_Fetch(DateCheckPic, Type)

        self.dob_result = Validations.DoB_Fetch(self.predictions.dob, Type)

        # If either DoB cannot be read, don't bother about the date check method
        if self.bardob_result == 'Could Not Validate' or self.dob_result == 'Could Not Validate':
            DateCheck = 'Could Not Validate'
            self.date_result = DateCheck
            
        # If both contain digits, perform the date check
        else:
            self.date_result = Validations.date_check(self.bardob_result, self.dob_result)

        self.extra_result, last3 = Validations.ExtraDocument_Validate(LastThreePic, Type)

        self.gender_result = Validations.Gender_Validate(date_digits, Type)

        self.nationality_result = Validations.Nationality_Validate(last3, Type)

    def Barcode_Find(self, Barcode):
        # load the image and convert it to grayscale
        image = Barcode
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # compute the Scharr gradient magnitude representation of the images in both the x and y direction using OpenCV 2.4
        ddepth = cv2.cv.CV_32F if imutils.is_cv2() else cv2.CV_32F
        gradX = cv2.Sobel(gray, ddepth=ddepth, dx=1, dy=0, ksize=-1)
        gradY = cv2.Sobel(gray, ddepth=ddepth, dx=0, dy=1, ksize=-1)

        # subtract the y-gradient from the x-gradient
        gradient = cv2.subtract(gradX, gradY)
        gradient = cv2.convertScaleAbs(gradient)

        # blur and threshold the image
        blurred = cv2.blur(gradient, (5, 5))
        (_, thresh) = cv2.threshold(blurred, 225, 255, cv2.THRESH_BINARY)

        # construct a closing kernel and apply it to the thresholded image
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 7))
        closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

        # perform a series of erosions and dilations
        closed = cv2.erode(closed, None, iterations = 4)
        closed = cv2.dilate(closed, None, iterations = 4)

        # find the contours in the thresholded image, then sort the contours by their area, keeping only the largest one
        cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        if cnts != []:
            c = sorted(cnts, key = cv2.contourArea, reverse = True)[0]

            count = 0
            for each in c:
                for one in each:
                    count += 1
            
            if count >= 30:
                self.barcode_result = 'Barcode_Found'
                return

        self.barcode_result = 'Could Not Validate'
        return

    def Extract_All_Characters(self, file, Type):
        #Read in image in grayscale
        imgLoad = file
        #split into the different functionalities for post2010 and pre2010 ID Books
        if Type == 0:
            self.Extract_ROIs(imgLoad, 0)
            self.ROI_Thresh(0)
            images = self.Extract_Characters(0)
            self.find_and_validate_emblems(imgLoad, 0)
            self.Barcode_Validation(imgLoad, self.threshed[2], 0)
            del self.threshed
            self.Barcode_Find(self.barcode_pic)
        else:
            self.Extract_ROIs(imgLoad, 1)
            self.ROI_Thresh(1)
            images = self.Extract_Characters(1)
            self.find_and_validate_emblems(imgLoad, 1)
            self.Barcode_Validation(imgLoad, self.threshed[2], 1)
            del self.threshed
            self.Barcode_Find(self.barcode_pic)

        TotalFailed = 0

        self.emblems_found = str(self.emblems_found)

        if self.emblems_found == '1' or self.emblems_found == '0':
            TotalFailed += 1
        if self.extra_result == 'Could Not Validate':
            TotalFailed += 1
        if self.barcode_result == 'Could Not Validate':
            TotalFailed += 1
        if self.nationality_result == 'Could Not Validate':
            TotalFailed += 1
        if self.date_result == 'Could Not Validate':
            TotalFailed += 1
        if self.gender_result == 'Could Not Validate':
            TotalFailed += 1
        if len(self.idNumber) != 13:
            TotalFailed += 1

        self.tests_passed = str(7-TotalFailed)

        self.validations = Val_Text_maker(self)
        self.images = Image_maker(images, self.barcode_pic)

        object = self

        del self

        return object

class Text_maker():
    def __init__(self, texts):
        count = 0
        for text in texts:
            if count == 0:
                self.cob = text
            elif count == 1:
                self.doi = text
            elif count == 2:
                self.dob = text
            elif count == 3:
                self.name = text
            elif count == 4:
                self.surname = text
            count += 1

class Image_maker():
    def __init__(self, images, barcode):
        retval, buffer = cv2.imencode('.png', barcode)
        img_as_text = base64.b64encode(buffer)
        self.barcode_pic = img_as_text
        count = 0
        for image in images:

            retval, buffer = cv2.imencode('.png', image)
            img_as_text = base64.b64encode(buffer)

            if count == 0:
                self.cob = img_as_text

            elif count == 1:
                self.doi = img_as_text

            elif count == 2:
                self.dob = img_as_text

            elif count == 3:
                self.name = img_as_text

            elif count == 4:
                self.surname = img_as_text
            count += 1

class Val_Text_maker():
    def __init__(self, texts):
        self.emblems_found = texts.emblems_found
        self.barcode_validation = texts.barcode_result
        self.date_validation = texts.date_result
        self.extra_documents_needed = texts.extra_result
        self.nationality = texts.nationality_result
        self.gender = texts.gender_result
        self.tests_passed = texts.tests_passed
        self.idNumber = texts.idNumber
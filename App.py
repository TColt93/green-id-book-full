#python3.6 App.py -i "michael meyer.jpg"
from datetime import datetime 
import os
import cv2
import numpy as np
import argparse
from random import randint
import os
import Green_ID_OCR as OCR
import homography
import json
import base64

class GreenID():
    def __init__(self, idImage, debug):
        self.debug = debug

        self.idImage = idImage

        self.ocr_images = None
        self.ocr_predictions = None
        self.validation_images = None
        self.validation_predictions = None
        self.valid = True

    # This function is used to do validations and OCR of the given files
    # Returns the consolidated predictions and consolidated image arrays
    def Full_ID(self):
        # Read in the orginial image from file path in color
        img = cv2.imread(self.idImage, cv2.IMREAD_COLOR)

        if self.valid == True:
            # Input the image we have just read into the Homography function
            # Return best image and type of ID Book
            bestImage, Type = homography.Perfect_Image(img)

            # Input the best image and ID Type to the OCR function to extract all text and numbers
            # Return the OCRImg array with bounding boxes and character predictions and the characters extracted in json format
                        
            ocr = OCR.GreenID_OCR()
            OCR_self = ocr.Extract_All_Characters(bestImage, Type)
            

            if len(OCR_self.predictions.dob) != 10 and len(OCR_self.idNumber) == 13:
                if OCR_self.idNumber[0] in ['0','1','2']:
                    OCR_self.predictions.dob = '20{}-{}-{}'.format(OCR_self.idNumber[:2],OCR_self.idNumber[2:4],OCR_self.idNumber[4:6])
                else:
                    OCR_self.predictions.dob = '19{}-{}-{}'.format(OCR_self.idNumber[:2],OCR_self.idNumber[2:4],OCR_self.idNumber[4:6])
     
            if OCR_self.nationality_result == 'SA Citizen' or ((OCR_self.idNumber == 13) and (OCR_self.idNumber[10] == '0')):
                OCR_self.predictions.cob = 'SOUTH AFRICA'

            self.predictions = OCR_self.predictions
            self.images = OCR_self.images
            self.tests_passed = OCR_self.tests_passed
            self.idNumber = OCR_self.idNumber
            self.TestId = self.idImage.replace('\\front.jpeg', '').replace('Package\\','')

            del OCR_self

        else:
            self.predictions = 'This image is not suitable to be processed'

        if self.debug == 'True':
            # folderNumber = Folder_maker()
            barcode = Image_base64_Decoder(self.images.barcode_pic)
            Image_Writer(barcode, 'barcode')
            cob = Image_base64_Decoder(self.images.cob)
            Image_Writer(cob, 'cob')
            doi = Image_base64_Decoder(self.images.doi)
            Image_Writer(doi, 'doi')
            dob = Image_base64_Decoder(self.images.dob)
            Image_Writer(dob, 'dob')
            name = Image_base64_Decoder(self.images.name)
            Image_Writer(name, 'name')
            surname = Image_base64_Decoder(self.images.surname)
            Image_Writer(surname, 'surname')

    def Function_Chooser(self):

        # Run the Full_ID Functionality
        GreenID.Full_ID(self)
        
        if self.valid == True:

            response = Response_maker(self.predictions, self.images, self.tests_passed, self.idNumber, self.TestId)

        return response

class Response_maker():
    def __init__(self, preds, imgs, tests, idNum, folderNum):
        self.predictions = preds
        self.images = imgs
        self.tests_passed = tests
        self.idNumber = idNum
        self.folderNumber = folderNum

def Image_base64_Decoder(image):

    idimage = base64.b64decode(image)
    idnparr = np.frombuffer(idimage, np.uint8)

    return cv2.imdecode(idnparr, cv2.IMREAD_COLOR)

def Image_Writer(image, extension):
    cv2.imwrite('debug/{}.png'.format(extension), image)

def Folder_maker():
    counter = 0
    directory = 'debug/'
    files = []
    folderNumber = 0
    for folder in os.listdir(directory):
        files.append(folder)

    while counter != len(files):
        folderNumber = int(folder.replace('/', '').replace('Test', ''))
        counter += 1
    
    folderNumber += 1
    if not os.path.exists(directory + 'Test{}'.format(str(folderNumber))):
        os.makedirs(directory + 'Test{}'.format(str(folderNumber)))

    return folderNumber

####### -------------------------------- Call Method -------------------------------- #######
 
# Initialize the argeparse for the required inputs
ap = argparse.ArgumentParser()
predictions = []
outputs = []
ap.add_argument("-i", "--image",
    default=None, help="single image")
ap.add_argument("-d", "--debug",
    default=True, help="return images")
args = vars(ap.parse_args())

# Pass through the arguments and make a decision whether or not to iterate through a folder or append a file
if args["image"] != None:
    image = args["image"]

# Create a returns variable to understand whether or not the call expects image outputs
debug = args["debug"]

greenIdOcr = GreenID(image, debug)
obj = greenIdOcr.Function_Chooser()

tests = str(round(int(obj.tests_passed)/7*100)) + '%'

print('{},{},{},{},{},{},{},{}'.format(obj.folderNumber, obj.idNumber, obj.predictions.surname, obj.predictions.name, obj.predictions.cob, obj.predictions.dob, obj.predictions.doi, tests))

####### -------------------------------- END -------------------------------- #######

import cv2
import numpy as np
import argparse
from random import randint
import os
 
MAX_FEATURES = 2000
GOOD_MATCH_PERCENT = 0.075

def align_images(im1, im2):
    #Make the Template Image the standard size to ensure the best results when finding which Template to use
    shape = im2.shape
    height = 650
    r = height / float(shape[0])
    dim = (int(shape[1] * r), height)

    im2 = cv2.resize(im2, dim, interpolation = cv2.INTER_AREA)

    # Convert images to grayscale
    im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

    # Detect ORB features and compute descriptors.
    orb = cv2.ORB_create(MAX_FEATURES)
    keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
    keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)

    # Match features.
    matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
    matches = matcher.match(descriptors1, descriptors2, None)

    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=False)

    # Remove not so good matches
    numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:numGoodMatches]

    # Draw top matches (Use for testing purposes)
    # imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
    # cv2.imwrite("matches.jpg", imMatches)

    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)

    for i, match in enumerate(matches):
        points1[i, :] = keypoints1[match.queryIdx].pt
        points2[i, :] = keypoints2[match.trainIdx].pt

    # Find homography
    h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)

    # Use homography
    height, width, channels = im2.shape
    im1Reg = cv2.warpPerspective(im1, h, (width, height))

    c = 0
    total = 0
    for match in matches[:10]:
        total = total + match.distance
        c += 1

    median = total/c

    return [im1Reg, median]

count = 0

def Perfect_Image(file):
    Images = []
    References = []
    # Read image to be aligned
    im = file

    # Read reference image post 2010
    refFilename = "Templates/Post2010.png"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    # Read reference image post 2010
    refFilename = "Templates/Post2010_1.jpeg"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    # Read reference image pre 2010
    refFilename = "Templates/Post2010_2.jpg"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    # Read reference image pre 2010
    refFilename = "Templates/Pre2010.jpg"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    # Read reference image pre 2010
    refFilename = "Templates/Pre2010_1.jpg"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    # Read reference image with the old badge
    refFilename = "Templates/OldBadge.png"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    # Read reference image with the old badge
    refFilename = "Templates/OldBadge_1.jpg"
    References.append(cv2.imread(refFilename, cv2.IMREAD_COLOR))

    for reference in References:
        # Registered image will be resotred in imReg.
        # The estimated homography will be stored in h.
        Images.append(align_images(im, reference))

    bestDist = 1000
    cnt = 0
    cntr = 0
    for Image in Images:
        if Image[1] < bestDist:
            bestDist = Image[1]
            bestImage = Image[0]
            cntr = cnt
        cnt += 1
    if cntr < 3:
        Type = 0
    else:
        Type = 1

    # cv2.imshow(str(bestDist), bestImage)
    # cv2.waitKey(0)

    return bestImage, Type
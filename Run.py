import os
import App
from datetime import datetime

directory = 'Package/'
files = []

for folder in os.listdir(directory):
    for file in os.listdir(directory+folder):
        if 'front' in file:
            if '.jpeg' in file or '.jpg' in file or '.png' in file:
                files.append(directory+folder+ '/' + file)

for file in files:
    starttime = datetime.now()
    greenIdOcr = App.GreenID(file, 'True')
    obj = greenIdOcr.Function_Chooser()
    tests = str(round(int(obj.tests_passed)/7*100)) + '%'
    endtime = datetime.now()
    print(file.replace(directory, '').replace('front.jpeg', '') + ' - ' + str(endtime-starttime))
    print('{},{},{},{},{},{},{}'.format(obj.idNumber, obj.predictions.surname, obj.predictions.name, obj.predictions.cob, obj.predictions.dob, obj.predictions.doi, tests))

    del obj